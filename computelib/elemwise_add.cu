#include <cstdio>
#include <iostream>

#include "computelib.h"

__global__ void add_kernel(int* c, int const* a, int const* b) {
    const int i = threadIdx.x;
    c[i] = a[i] + b[i];
}

cudaError_t add_with_cuda(int* c, const int* a, const int* b, const uint32_t size) {
    int* dev_a = nullptr;
    int* dev_b = nullptr;
    int* dev_c = nullptr;

    cuda_call(cudaSetDevice(0));

    cuda_call(cudaMalloc(&dev_a, size * sizeof(int)));
    cuda_call(cudaMalloc(&dev_b, size * sizeof(int)));
    cuda_call(cudaMalloc(&dev_c, size * sizeof(int)));

    cuda_call(cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice));
    cuda_call(cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice));

    // block/thread parallel
    add_kernel << <1, size >> > (dev_c, dev_a, dev_b);

    cuda_call(cudaDeviceSynchronize());
    cuda_call(cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost));

    cudaFree(dev_a);
    cudaFree(dev_b);
    cudaFree(dev_c);

    return cudaError_t();
}
