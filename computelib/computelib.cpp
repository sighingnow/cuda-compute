#include "computelib.h"

cudaError_t cuda_call(const cudaError_t cuda_error, const char *msg) {
    if (cuda_error != cudaSuccess) {
        if (msg) {
            fprintf(stderr, "cuda error: %s\n", msg);
        }
        else {
            fprintf(stderr, "cuda error: %s\n", cudaGetErrorString(cuda_error));
        }
    }
    return cuda_error;
}

void cuda_check(const cudaError_t cuda_error, const char *msg) {
    if (cuda_error != cudaSuccess) {
        if (msg) {
            fprintf(stderr, "cuda error: %s\n", msg);
        }
        else {
            fprintf(stderr, "cuda error: %s\n", cudaGetErrorString(cuda_error));
        }
        std::exit(cuda_error);
    }
}
