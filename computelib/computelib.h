#pragma once

#ifndef CUDA_COMPUTE_COMPUTELIB_H
#define CUDA_COMPUTE_COMPUTELIB_H

#include <cstdint>
#include <cstdio>
#include <cstdlib>

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

cudaError_t cuda_call(const cudaError_t cuda_error, const char *msg = nullptr);
void cuda_check(const cudaError_t cuda_error, const char *msg = nullptr);

cudaError_t add_with_cuda(int* c, const int* a, const int* b, const uint32_t size);

#endif // CUDA_COMPUTE_COMPUTELIB_H
