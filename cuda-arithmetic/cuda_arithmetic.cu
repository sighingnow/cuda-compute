#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>

#include "computelib.h"

int main() {
    const uint32_t array_size = 5;
    const int a[array_size] = { 1, 2, 3, 4, 5 };
    const int b[array_size] = { 10, 20, 30, 40, 50 };
    int c[array_size] = { 0 };

    cuda_call(add_with_cuda(c, a, b, array_size));

    printf("{1,2,3,4,5} + {10,20,30,40,50} = {%d,%d,%d,%d,%d}\n",
        c[0], c[1], c[2], c[3], c[4]);

    cuda_call(cudaDeviceReset());

    thrust::host_vector<int> host_vec(1 * 1024 * 1024);
    thrust::generate(std::begin(host_vec), std::end(host_vec), std::rand);
    
    thrust::device_vector<int> dev_vec = host_vec;

    thrust::sort(std::begin(dev_vec), std::end(dev_vec));
    thrust::copy(std::begin(dev_vec), std::end(dev_vec), std::begin(host_vec));

    for (int i = 0; i < 10; ++i) {
        printf("%d ", host_vec[1024 + i]);
    }

    return 0;
}
